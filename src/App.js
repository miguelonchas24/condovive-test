import React,{ useEffect, useState } from 'react';
import axios from 'axios';
import './App.css';

const App = () => {

  const [users, setUsers] = useState([])
  const [filerUsers, seFiltertUsers] = useState([])

  const getUserList = async () => {    
    return await axios.get('https://reqres.in/api/users')    
  }

  const setUsersList = async () => {
    const response = await getUserList() 
    if(response.request.status === 200){
      setUsers(response.data.data)
    } else {
      setUsers([])
    }
  }

  const filterUsers = () => {
    console.log(users)
    setUsers([])    
    
    let auxUsers = [...users].sort((a,b) => {
      if(a.last_name > b.last_name)
      return 1
      else if (a.last_name < b.last_name)
      return -1
      else 
      return 0
    })    
    setUsers(auxUsers)
    
  }
  
  
  const filterUsersDesc = () => {
    console.log(users)
    setUsers([])
    let auxUsers = [...users].sort((a,b) => {
      if(a.last_name < b.last_name)
      return 1
      else if (a.last_name > b.last_name)
      return -1
      else 
      return 0
    })    
    setUsers(auxUsers)
  }

  useEffect(() => {
    setUsersList()    
  },[])
  
  return (
    <div className=""> 
    <div>
      <button onClick={filterUsers}>Ascending</button>
      <button onClick={filterUsersDesc}>Descending</button>
      </div>             
        <div>
          {users?.map((user) => (
            <div className='user-container' >
              <img src={user.avatar} alt="" />
              <p>{user.first_name}</p>
              <p>{user.last_name}</p>
              <p>{user.email}</p>              
            </div>            
          ))}
        </div>

        {/* <div>
          {filerUsers.map((user) => (
            <div className='user-container' >
              <img src={user.avatar} alt="" />
              <p>{user.first_name}</p>
              <p>{user.last_name}</p>
              <p>{user.email}</p>              
            </div>            
          ))}
        </div> */}

        {JSON.stringify(users)}
    </div>
  );
}

export default App;
